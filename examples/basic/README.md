# Basic use case

This example shows a basic use case:

- Initialising the Product Flow client
- Creating an access controller
- Checking if a feature is enabled
