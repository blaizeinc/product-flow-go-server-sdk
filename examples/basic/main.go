package main

import (
	"fmt"
	"os"

	productflow "bitbucket.org/blaizeinc/product-flow-go-server-sdk"
)

// Add your credentials here
const (
	orgId       = ""
	jwt         = ""
	featureSlug = ""
)

func main() {

	if orgId == "" || jwt == "" || featureSlug == "" {
		fmt.Println("add missing credentials to continue.")
		os.Exit(1)
	}

	fmt.Println("creating new Product Flow client...")

	zclient, err := productflow.NewClient(orgId)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("starting new access controller...")

	accParams := productflow.AccessParams{Jwt: jwt}
	customInputs := map[string]interface{}{"tasks": 3}

	access, err := zclient.StartAccessController(accParams, customInputs)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("access controller started:", access.Started)

	if access.IsFeatureEnabled(featureSlug, customInputs, false) {
		fmt.Println(featureSlug, "is enabled!")
	} else {
		fmt.Println(featureSlug, "is disabled!")
	}

	fmt.Println("done.")
}
