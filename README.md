# Product Flow Go Server SDK

A Go server-side SDK to handle Product Flow features.

This SDK works with Go 1.17 and higher.

## Getting started

### Install the SDK

```go
go get bitbucket.org/blaizeinc/product-flow-go-server-sdk
```

### Import the SDK

```go
import (
    productflow "bitbucket.org/blaizeinc/product-flow-go-server-sdk"
)
```

### Initialise the client

Create only one client instance in your application.

```go
client, err := productflow.NewClient("yourOrgId")
```

### Create an access controller for request handling

An access controller allows you to handle different user requests.

```go
accParams := productflow.AccessParams{
    Jwt: "..." // User JWT token controls access, required
}
customInputs := map[string]interface{}{
    "tasks": 3 // 0 or more custom inputs can be passed
}

// Create and start an Access Controller
access, err := client.StartAccessController(accParams, customInputs)
```

### Check feature access

```go
// Check feature access by passing the feature slug, any custom inputs needed and a default enabled state
isEnabled := access.IsFeatureEnabled("yourFeatureSlug", customInputs, false)
if isEnabled {
    // Handle feature enabled
} else {
    // Handle feature disabled
}
```

### Get rule decisions

```go
// Get rule decisions with raw decision output
output, err := ac.GetRuleDecision("rule", customInputs)

// Get rule decisions with output value converted to native Go types
outputString, err := ac.GetRuleDecisionString("rule-string", customInputs)
outputInt, err := ac.GetRuleDecisionInt("rule-int", customInputs)
outputFloat, err := ac.GetRuleDecisionFloat("rule-float", customInputs)
```

### Report user tier change

```go
// Pass through a users previous tier to track changes
err := ac.ReportUserTierChange("previousTierSlug")
```

## Examples

Example implementations can be found in the `examples` directory.
